import React, { Component } from 'react'
import { Image, ImageBackground, ScrollView, Text, View, ViewComponent } from 'react-native'
import { Avatar } from 'react-native-elements';

const image = { uri: 'https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-9/117145229_3184799721611209_1275469269847026849_n.jpg?_nc_cat=101&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeFzmPZWMstcwfKvHYOiTCHvi258ZPh4Ef-Lbnxk-HgR_29PIj9ooeI-IXvzWHD2Br9esaeO-MfMAyqTM-e_Ni0J&_nc_ohc=47LqHIJ8rNQAX8d-VIQ&_nc_ht=scontent.fpnh10-1.fna&oh=5fc47c4ba88cae7c67a0336f72c1ed18&oe=5FDAA55F' };

export class Story extends Component {
    render() {
        return (

         

                    <ImageBackground
                        source={ {uri:this.props.story_img}}
                        style={{ width: 100, height: 140 ,margin:2 }}
                        imageStyle={{ borderRadius: 5}}
                    >
                        <Avatar
                            containerStyle={{ margin: 5, borderColor: "white", borderWidth: 1 }}
                            rounded
                            source={{
                                uri: this.props.profile_img                  
                            }}
                        >
                        </Avatar>
                        <Text style={{color:"white",bottom:-70,marginLeft:5,fontSize:12,fontWeight:"bold"}}>{this.props.profile_name}</Text>
                    </ImageBackground>
               
     


        )
    }
}

export default Story
