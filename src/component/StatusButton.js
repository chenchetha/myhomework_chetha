import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { Button, Icon } from 'react-native-elements'

export class StatusButton extends Component {
    render() {
        return (
                <View style={{ flexDirection: "row"}}>
                    <View style={{ width: 100, height: 35, flex: 1 }}>
                        <Button
                            title="Live"
                            type="clear"
                            titleStyle={{ color: "blue" }}
                            icon={
                                <Icon
                                    name='videocam'
                                    type='material'
                                    size={20}
                                    color="blue"
                                    containerStyle={{ marginRight: 5 }}
                                />
                            }
                        />
                    </View>
                    <View style={{ width: 100, height: 35, flex: 1 }}>
                        <Button
                            title="Photo"
                            type="clear"
                            titleStyle={{ color: "blue" }}
                            icon={
                                <Icon
                                    name='photo'
                                    type='material'
                                    size={20}
                                    color="blue"
                                    containerStyle={{ marginRight: 5 }}
                                />
                            }
                        />
                    </View>
                    <View style={{ width: 100, height: 35, flex: 1 }}>
                        <Button
                            title="Check In"
                            type="clear"
                            titleStyle={{ color: "blue" }}
                            icon={
                                <Icon
                                    name='place'
                                    type='material'
                                    size={20}
                                    color="blue"
                                    containerStyle={{ marginRight: 5 }}
                                />
                            }
                        />
                    </View>
                </View>
        )
    }
}

export default StatusButton
