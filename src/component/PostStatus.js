import React, { Component } from 'react'
import { Image, Text, View } from 'react-native'
import { Avatar, Divider, Icon } from 'react-native-elements'

export class PostStatus extends Component {
    render() {
        return (
            <View>
                <View style={{ flexDirection: "row", marginTop: 5 }}>
                    <View>
                        <Avatar
                            containerStyle={{ margin: 5, borderColor: "white", borderWidth: 1 }}
                            rounded
                            source={{
                                uri:  this.props.profile_img
                            }}
                        >
                        </Avatar>
                    </View>
                    <View>
                        <Text style={{ fontWeight: "bold",marginTop:3 }}>{this.props.profile_name}</Text>
                        <Text style={{ fontSize: 10, color: "gray" }}>{this.props.duration_post}</Text>
                    </View>
                    <View style={{flex:1,alignItems:"flex-end"}}>
                        <Icon
                            name='more-horiz'
                            type='material'
                            size={25}
                            color="black"
                            containerStyle={{ margin: 5 }}
                        />
                    </View>
                </View>
                <View >
                    <View style={{ padding: 5 }}>
                        <Text>{this.props.caption}</Text>
                    </View>
                    <View>
                        <Image
                            style={{ width: "100%", height: 225, resizeMode: 'cover' }}
                            source={{ uri: this.props.new_feed_img }}
                        >
                        </Image>
                        <View style={{ flex: 1, flexDirection: "column" }}>
                            <View style={{ height: 30, flexDirection: "row" }}>
                                <View style={{ flex: 1, height: 30 }}>
                                    <Text style={{ fontWeight: "bold", color: "gray", margin: 5 }}>Messager</Text>
                                </View>
                                <View style={{ flex: 1, height: 30, alignItems: "flex-end" }}>
                                    <Icon
                                        name='bookmark'
                                        type='material'
                                        size={20}
                                        color="gray"
                                        containerStyle={{ margin: 5 }}
                                    />
                                </View>
                            </View>
                            <View style={{ height: 30, flexDirection: "row" }}>
                                <View style={{ flex: 1, height: 30, flexDirection: "row" }}>
                                    <Icon
                                        name='favorite-border'
                                        type='material'
                                        size={22}
                                        color="gray"
                                        containerStyle={{ margin: 5 }}
                                    />
                                    <Text style={{ fontWeight: "bold", color: "gray", margin: 5 }}>{this.props.like_amount}</Text>
                                </View>
                                <View style={{ flex: 1, height: 30, flexDirection: "row" }}>
                                    <Icon
                                        name='comment-bank'
                                        type='material'
                                        size={20}
                                        color="gray"
                                        containerStyle={{ margin: 5 }}
                                    />
                                    <Text style={{ fontWeight: "bold", color: "gray", margin: 5 }}>{this.props.comment_amount}</Text>
                                </View>
                                <View style={{ flex: 1, height: 30, flexDirection: "row", justifyContent: "flex-end" }}>
                                    <Text style={{ fontWeight: "bold", color: "gray", margin: 5 }}>{this.props.shared_amount}</Text>
                                </View>
                            </View>
                        </View>

                    </View>
                </View>
                <Divider style={{ backgroundColor: 'gray',marginTop:5 }} />
            </View>
        )
    }
}

export default PostStatus
