import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { Avatar } from 'react-native-elements'

export class Status extends Component {
    render() {
        return (
                <View style={{  flexDirection: "row" ,marginLeft:10}}>
                    <View>
                        <Avatar
                            rounded
                            source={{
                                uri:
                                    'https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-9/117145229_3184799721611209_1275469269847026849_n.jpg?_nc_cat=101&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeFzmPZWMstcwfKvHYOiTCHvi258ZPh4Ef-Lbnxk-HgR_29PIj9ooeI-IXvzWHD2Br9esaeO-MfMAyqTM-e_Ni0J&_nc_ohc=47LqHIJ8rNQAX8d-VIQ&_nc_ht=scontent.fpnh10-1.fna&oh=5fc47c4ba88cae7c67a0336f72c1ed18&oe=5FDAA55F',
                            }}
                        >
                        </Avatar>
                    </View>
                    <View>
                        <Text style={{ color: "blue", margin: 10 }}>What's on your mind?</Text>
                    </View>
                </View>
        )
    }
}

export default Status
