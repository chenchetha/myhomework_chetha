

export const data_story = [
    {
        profile_id: 1,
        profile_name: "Chen Chetha",
        profile_img: "https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-9/117145229_3184799721611209_1275469269847026849_n.jpg?_nc_cat=101&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeFzmPZWMstcwfKvHYOiTCHvi258ZPh4Ef-Lbnxk-HgR_29PIj9ooeI-IXvzWHD2Br9esaeO-MfMAyqTM-e_Ni0J&_nc_ohc=47LqHIJ8rNQAX8d-VIQ&_nc_ht=scontent.fpnh10-1.fna&oh=5fc47c4ba88cae7c67a0336f72c1ed18&oe=5FDAA55F",
        story_img: "https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-9/126255131_3719717344734612_8858866190638552538_o.jpg?_nc_cat=107&ccb=2&_nc_sid=5b7eaf&_nc_eui2=AeFVDsc9MoAG4-c4O0sfgKfobDWNpFZPjeFsNY2kVk-N4cnt4H5CJLh0y1y4T5Xljs7q0LMx2OxugLAR3FijYJTB&_nc_ohc=dsv123cEJ1kAX8-hDvm&_nc_ht=scontent.fpnh10-1.fna&oh=dcd85eeebc387cbf4a528320156efee0&oe=5FDD6CEA"
    },
    {
        profile_id: 2,
        profile_name: "Jean",
        profile_img: "https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-9/118489743_3489637461116031_3736103923923519765_n.jpg?_nc_cat=108&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeHYzqEANPZTqra3IbQ2PiGIHYWW2OV8C2IdhZbY5XwLYjrG-RelWjmHtMsh4o9_BtmjsJsFS7fjCc3erKBBd2bT&_nc_ohc=_4z7-wtcsHwAX8huxq8&_nc_ht=scontent.fpnh10-1.fna&oh=9dda42ba27f171fa9757d425dba07d49&oe=5FDA133E",
        story_img: "https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-9/s1080x2048/126232492_3741257122620729_7643283814499467138_o.jpg?_nc_cat=100&ccb=2&_nc_sid=5b7eaf&_nc_eui2=AeGSS5zUfL_ZRnxGBSDDyG2zVmvLqALqjqxWa8uoAuqOrBCey_qiEgTXFs3ReqfJbeRYt5Mzs9rlTzKHjd8xVJnJ&_nc_ohc=yzrSfAd-CcsAX8cXo76&_nc_ht=scontent.fpnh10-1.fna&tp=7&oh=e44dc046efc85ec5af0919a360819894&oe=5FDA4B2D"
    },
    {
        profile_id: 3,
        profile_name: "Chetra Khim",
        profile_img: "https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-9/117106072_1734359293369232_3296305947001989707_n.jpg?_nc_cat=105&ccb=2&_nc_sid=174925&_nc_eui2=AeFFQQYzDFR7FLcgsRrmPmSxOcHYjC40lFA5wdiMLjSUUM-IpXMVywNkDuumPg_s3WMn7csis_x_AO4Zwa5rJtv9&_nc_ohc=N2FJBrBwHygAX-XZ8gk&_nc_ht=scontent.fpnh10-1.fna&oh=7b9468ab1821040a3315aceca2491555&oe=5FDA6374",
        story_img: "https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-9/s1080x2048/125215876_1838924156246078_1007315729616281810_o.jpg?_nc_cat=109&ccb=2&_nc_sid=5b7eaf&_nc_eui2=AeHdgc3Xmt5wQw-d0sFps4qo9cDtRXMbzQL1wO1FcxvNArk_hXlTlHrIbLO3mjVg7S1tEwLi3px_pZKVhwOETVHW&_nc_ohc=RlnCvsX5p6EAX8qt9mc&_nc_ht=scontent.fpnh10-1.fna&tp=7&oh=483f06db2cdfc3150cedef96ea8227ec&oe=5FDA5243"
    },
    {
        profile_id: 4,
        profile_name: "Long Cruise",
        profile_img: "https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-9/122237520_1016254445556164_832426053101812895_o.jpg?_nc_cat=105&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeEhw9vwEwMAFGFGgiUFcu7ibRMU6Q4dG_RtExTpDh0b9A4oUYUITdAzpFEUSxkbxJ0dOGS2hVmn-zbYaUDoXIkK&_nc_ohc=hRDYUvSJk6wAX-bfI0R&_nc_ht=scontent.fpnh10-1.fna&oh=827283caab3a0f028b388fa69e0ab26f&oe=5FDC4507",
        story_img: "https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-9/s1080x2048/126454167_1037596400088635_6783896673099926438_o.jpg?_nc_cat=109&ccb=2&_nc_sid=5b7eaf&_nc_eui2=AeHUNkWREGkYjToXhhVfwTXSZrOF8ISylGxms4XwhLKUbCtKU6JlOANFLYxibl0xX4olzuePepg8YLvue-qec-Rp&_nc_ohc=inWa4D7LhREAX95VYo8&_nc_ht=scontent.fpnh10-1.fna&tp=7&oh=38fc1473944c9197679578f2afbf823f&oe=5FDD4B65"
    },
    {
        profile_id: 5,
        profile_name: "អា ណែត",
        profile_img: "https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-9/124341316_848196089322462_4119055609396760016_n.jpg?_nc_cat=107&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeGaI04Rc6xZKPk5B3hScSprcZwpsKH4Th9xnCmwofhOH_qeaqzI6zZfhpH-8edUJLS6NIJCUDC3pIn44jTWG3Bq&_nc_ohc=N4eGD8KMVv8AX8KweiV&_nc_ht=scontent.fpnh10-1.fna&oh=2bf0753f97f90be8a153de7109afc1c6&oe=5FDDB16F",
        story_img: "https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-9/s1080x2048/125380688_856451248496946_8015371584692755709_o.jpg?_nc_cat=108&ccb=2&_nc_sid=5b7eaf&_nc_eui2=AeFyrBtpHSZV-RCi2d3czU6CPFGN7n0Ak6M8UY3ufQCTo0pXRY4tEA8WgjMNJ0lPt_b8Gk4bTxavbPxu2UPK9fyO&_nc_ohc=EFW_nY0mX64AX8yUAh5&_nc_ht=scontent.fpnh10-1.fna&tp=7&oh=31451ddb13901941201d510a15397d83&oe=5FDB2515"
    },
    {
        profile_id: 6,
        profile_name: "Jezzy MoMo",
        profile_img: "https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-9/93711499_3753282194744065_4023872911646392320_n.jpg?_nc_cat=103&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeF0SjjZUVf7VYnj-5YA3HyGQ6j2f68Eq7NDqPZ_rwSrs-3V64Db-L0Ebh-yce4yLQaktNZ9d4XTT7miS9JV47EL&_nc_ohc=Ypi5tWHhR0kAX9hqxu2&_nc_ht=scontent.fpnh10-1.fna&oh=ed5fe6ef05ab1a43d5a4caea86775c46&oe=5FDA3DB6",
        story_img: "https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-9/s1080x2048/125298576_4706276469444628_2519110773473939074_o.jpg?_nc_cat=100&ccb=2&_nc_sid=5b7eaf&_nc_eui2=AeFDsxMvZ1_08ZPhx9ZqCJOXdJ74FioZOEt0nvgWKhk4S5N19UGyY-hGxrMSkGKrH7y4Rw5184VwG7khyRVP4hGc&_nc_ohc=1HlNaEAathYAX-rQfVp&_nc_ht=scontent.fpnh10-1.fna&tp=7&oh=99e973a35b47f8fa580ba0abfa97f6e8&oe=5FDA9140"
    },
    {
        profile_id: 7,
        profile_name: "Peng Seyha",
        profile_img: "https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-9/123049109_3378698752225294_3560549193994860536_n.jpg?_nc_cat=109&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeHpWA3rOpls6HHVPmtgL8wrqJCablYycp6okJpuVjJynu6J6ZNPIKkVkPPE1JHyCS0qcyCMFkWe3t4x7Ru7Ov3P&_nc_ohc=60Dt69ozOs0AX9wpTiP&_nc_ht=scontent.fpnh10-1.fna&oh=08b079ca965d7b13a1aaf6d9322482be&oe=5FDCBF5D",
        story_img: "https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-9/s1080x2048/125177326_3440411036054065_1250283063205381443_o.jpg?_nc_cat=109&ccb=2&_nc_sid=5b7eaf&_nc_eui2=AeFSbrwRkRnGWq7XrtW9P45CP4-m9QvkaUY_j6b1C-RpRs27u7R0jtwDMoufKuQHFR7Bho5qDI-Dc9Ai-RFJW_yl&_nc_ohc=_2cF0Tz9CV8AX8O13PR&_nc_ht=scontent.fpnh10-1.fna&tp=7&oh=817ada1fc16ef75427fafa8af77dd098&oe=5FDA1727"
    },
    {
        profile_id: 8,
        profile_name: "Ninhh Ninhh",
        profile_img: "https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-9/123591881_2279403955539844_6172019250620599702_n.jpg?_nc_cat=100&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeEqD9C1eo0qVxEsk5mwlKAw8ey8JARIj2nx7LwkBEiPaQwpgXklObk6ilhDpYz0myxDXXZW2CEwtQk0SAvlC_8m&_nc_ohc=Bi-m-zs94T8AX-G1t6o&_nc_ht=scontent.fpnh10-1.fna&oh=bcfe6c9356d95c940a7a8aa5db5d35b4&oe=5FDDA29D",
        story_img: "https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-9/125474034_2297111930435713_1407257580407845303_o.jpg?_nc_cat=102&ccb=2&_nc_sid=5b7eaf&_nc_eui2=AeFrDgYu4gp75PHbH6cnQOxKl9KYPsSjy16X0pg-xKPLXpOPM4DMNi3Ky3NqcxdIjw0X6UvyXLdPOTtS2JDLn8e7&_nc_ohc=UX-pT9exS1AAX_GOPRX&_nc_ht=scontent.fpnh10-1.fna&oh=8cd5d5d47fe3a069c22a7f9670edd5c3&oe=5FDC17C0"
    }

]