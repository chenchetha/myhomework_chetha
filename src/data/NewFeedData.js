

export const data_newfeed = [
    {
        profile_id: 1,
        profile_name: "SreyLeak Chan",
        profile_img: "https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-9/122002014_1243968205983215_7281100905607969220_o.jpg?_nc_cat=106&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeFL5Np2XAMdC3PUO8TrJ3zZV0LmdQat8-JXQuZ1Bq3z4geAWEw4opmL_doQsBtER-YJQ38K98raXhq98t3Nw7uS&_nc_ohc=8NeMvl6BpsUAX8O4vil&_nc_ht=scontent.fpnh10-1.fna&oh=79f46f9a77630a65f5b6675b2fd868f1&oe=5FDD5E98",
        duration_post: "12 hrs",
        caption: "ពេលខ្លះពិបាកចិត្តពេករហូតដល់ថ្នាក់ញាុំបាយបណ្តើរ ដាក់ដាយចូលមាត់បណ្តើរ😓  ជូរចត់ណាស់ជីវិត😔 ចឹងបានវាមិនតូចដូចគេសោះ ធំឡើងកាមឹបៗៗៗៗ😬😂",
        new_feed_img: "https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-0/p526x296/125865316_1272260453153990_7252666423099466724_o.jpg?_nc_cat=110&ccb=2&_nc_sid=8bfeb9&_nc_eui2=AeGp0WCm6kPJkYBgZn_bmrKy7vAd7zOAkqvu8B3vM4CSq5Q5DRGXz3b9AOq8kTwak634hCC00nSc7Yk98xpZhlul&_nc_ohc=STuLXxTBDnUAX-QX5wo&_nc_ht=scontent.fpnh10-1.fna&tp=6&oh=f5f5138c9a1c260e1f104cc8d7b5b112&oe=5FDD2899",
        like_amount: "100K",
        comment_amount: "30K",
        shared_amount: "200K"
    },
    {
        profile_id: 2,
        profile_name: "Nith Nith",
        profile_img: "https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-9/123139885_100717755187944_9193478065752482120_o.jpg?_nc_cat=108&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeHAIjhw2SF4ePwpwUUyO7TbccCOODBYn6lxwI44MFifqeNcAjAZtg94-Lsn0p9DS6HSF0IbWINwSi0NRTnOnen6&_nc_ohc=cXDkBE2WPTAAX80Pv_w&_nc_ht=scontent.fpnh10-1.fna&oh=15373d19e9c48019cdd574ab2cdfd312&oe=5FDB0082",
        duration_post: "25 mn",
        caption: "មិនពេញចិត្តចេញ.....😒😒\n#single🥰🥰",
        new_feed_img: "https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-9/126862879_113643010562085_6375072908743888026_o.jpg?_nc_cat=111&ccb=2&_nc_sid=730e14&_nc_eui2=AeEf3E9-BXzAodD1TxafmSFotv-Ee8D9_3a2_4R7wP3_dowgofjFAzJJQi3mI4UNY33Q-25ckUZqA1gIQ-qWMMUl&_nc_ohc=Z-DWqb_GC04AX_kNYIj&_nc_ht=scontent.fpnh10-1.fna&oh=85ad2ce054c93e305733a63a19431a7a&oe=5FDCE14A",
        like_amount: "1000K",
        comment_amount: "500K",
        shared_amount: "2000K"
    },
    {
        profile_id: 3,
        profile_name: "Piich Pichhy",
        profile_img: "https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-9/126122439_1590303437837353_2957278445215871773_n.jpg?_nc_cat=109&ccb=2&_nc_sid=8bfeb9&_nc_eui2=AeFcSJdtj71gfs-Il_X6AxU2KD67lTJs5-YoPruVMmzn5kUtUQWdWrrdwDMatOo-XBt22Pp4tKNPOgT9_yR83ATy&_nc_ohc=8-H5HqJaFLgAX-bCha3&_nc_ht=scontent.fpnh10-1.fna&oh=f31eb16c4249cd139f8d286f77fc5aad&oe=5FDC0B17",
        duration_post: "15 hrs",
        caption: "Sometimes your heart needs more time to accept what your mind already knows 🙂",
        new_feed_img: "https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-0/p526x296/126327919_1592237417643955_4421358506857743667_n.jpg?_nc_cat=103&ccb=2&_nc_sid=8bfeb9&_nc_eui2=AeHKh-o5UGoyVkQljZxeObOTQtowK3SiHERC2jArdKIcRM5ZSE9kUSX8zbPM6UaXXypbHsBvngJvcfa_2y4Sk7-b&_nc_ohc=zsvrc4fkHdYAX_plocC&_nc_ht=scontent.fpnh10-1.fna&tp=6&oh=2cfc76d72e731d4a218f5812ae888cd3&oe=5FDBE14D",
        like_amount: "900K",
        comment_amount: "20K",
        shared_amount: "30K"
    },
    {
        profile_id: 4,
        profile_name: "Möuy Möuy",
        profile_img: "https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-9/122303180_2784068598545787_7838633325919996073_n.jpg?_nc_cat=103&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeES1uBLtPrmq4l0ZXd_FyVosnSEW52f71OydIRbnZ_vU_KpRh9PMdAXmpw45nojuzZ-V-pGvZTnovPHL8LHUq1N&_nc_ohc=uNM8AB8GN2cAX9Xdjux&_nc_ht=scontent.fpnh10-1.fna&oh=4b8fd8baf1847c9717df51afb0d5bb80&oe=5FDB4B35",
        duration_post: "20 mn",
        caption: "I always care about everyone. But who care about me?",
        new_feed_img: "https://scontent.fpnh10-1.fna.fbcdn.net/v/t1.0-0/p526x296/126051529_2807670829518897_7115368218886911367_o.jpg?_nc_cat=100&ccb=2&_nc_sid=8bfeb9&_nc_eui2=AeGetAnA4KOQEo5gqG2q5pXZtKJnpy6Cilq0omenLoKKWnnmQp0DGvPCt43EW1CRO38S9Ou4HN3MqOBSiTPSX-6J&_nc_ohc=reGLoYG5I2kAX8sjSDy&_nc_ht=scontent.fpnh10-1.fna&tp=6&oh=6463cb3eaa87f08372e887db84d824a4&oe=5FDE2C98",
        like_amount: "800K",
        comment_amount: "600K",
        shared_amount: "200K"
    }
]