import React, { Component } from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import { Header, Icon } from 'react-native-elements'
import PostStatus from '../component/PostStatus'
import Status from '../component/Status'
import StatusButton from '../component/StatusButton'
import Story from '../component/Story'
import { data_newfeed } from '../data/NewFeedData'
import { data_story } from '../data/StoryData'


const styles = StyleSheet.create({
    header_title: {
        left: -15, color: 'blue', fontSize: 27, fontWeight: "bold"
    },
    header_right_icon: {
        flex: 1,
        flexDirection: 'row',
        alignItems: "center"
    }
})

export class Home extends Component {
    render() {
        return (
             <View style={{flex: 1,flexDirection:"column",justifyContent:"flex-start"}}>
                  <View >
                    <Header
                        containerStyle={{
                            backgroundColor: 'white',
                            justifyContent: 'space-around',
                        }}
                        placement="left"
                        centerComponent={{ text: 'Facebook', style: styles.header_title }}
                        rightComponent={
                            <View style={styles.header_right_icon}>
                                <Icon
                                    name='favorite'
                                    type='material'
                                    color='blue'
                                    containerStyle={{ margin: 5 }}
                                />
                                <Icon
                                    name='search'
                                    type='material'
                                    color='blue'
                                    containerStyle={{ margin: 5 }}
                                />
                            </View>
                        }
                    />
                </View> 

            <ScrollView >
                <View >
                   <Status></Status>
                </View>
                <View >
                    <StatusButton></StatusButton>
                </View>
                <ScrollView  horizontal={true} showsHorizontalScrollIndicator={false} style={{ flexDirection: "row", marginTop: 5 }}>
                    {
                        data_story.map( item =>
                        <Story
                            key={item.profile_id}
                            profile_name={item.profile_name}
                            profile_img={item.profile_img}
                            story_img={item.story_img}
                        />                    
                     )
                    }
                 </ScrollView>
                 <View>
                    {
                        data_newfeed.map(item=> 
                            <PostStatus
                                  key={item.profile_id}
                                  profile_img={item.profile_img}
                                  profile_name={item.profile_name}
                                  duration_post={item.duration_post}
                                  new_feed_img={item.new_feed_img}
                                  caption={item.caption}
                                  like_amount={item.like_amount}
                                  comment_amount={item.comment_amount}
                                  shared_amount={item.shared_amount}
                            />
                            )
                    }              
                 </View>
            </ScrollView>
      

        </View> 
        )
    }
}

export default Home
